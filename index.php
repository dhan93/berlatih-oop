<?php
require_once "./animal.php";
require_once "./ape.php";
require_once "./frog.php";

$sheep = new Animal("Shaun");
$kodok = new Frog("buduk");
$sungokong = new Ape("Kera Sakti");

echo "name : ". $sheep->name; // "shaun"
echo "<br>legs : " . $sheep->legs; // 4
echo "<br>cold_blooded : " . $sheep->cold_blooded; // "no"
echo "<br><br>";
echo "name : ". $kodok->name;
echo "<br>legs : " . $kodok->legs;
echo "<br>cold_blooded : " . $kodok->cold_blooded;
echo "<br>Jump : " . $kodok->jump();
echo "<br><br>";
echo "name : ". $sungokong->name;
echo "<br>legs : " . $sungokong->legs;
echo "<br>cold_blooded : " . $sungokong->cold_blooded;
echo "<br>Yell : " . $sungokong->yell();
?>